<?php

/**
 * Class Api
 */
class Api
{

  /** @var array tables and required fields */
  public static $tables = [
    'News' => ['ParticipantId', 'NewsTitle', 'NewsMessage'],
    'Participant' => ['Email', 'Name'],
    'Session' => ['Name', 'TimeOfEvent', 'Description'],
    'Speaker' => ['Name'],
    'SessionSubscription' => ['SessionId', 'ParticipantId'],
  ];

  /** @var mysqli connection */
  private $_db;

  /** @var bool testing mode */
  public $testing = false;

  /**
   * Get $_POST or $_GET param by name
   *
   * @param $param
   * @param string $defaultValue
   * @return mixed
   */
  public function param($param, $defaultValue = '') {
    if (isset($_GET[$param])) return $_GET[$param];
    if (isset($_POST[$param])) return $_POST[$param];
    return $defaultValue;
  }

  /**
   * @return mixed
   */
  public function getDb() {
    if (!$this->_db) $this->_db = $this->connect();
    return $this->_db;
  }

  /**
   * connecting to db
   *
   * @return mysqli
   */
  private function connect() {
    //return $this->_db = mysqli_connect('127.0.0.1', 'root', 'q', 'test-api');
    return $this->_db = mysqli_connect('127.0.0.1', 'test-api', 'V3x6K3a0', 'test-api');
  }

  /**
   * Echo json result
   *
   * @param $data
   * @param string $message
   */
  public function response($data = [], $message = '') {
    header('Content-Type: application/json');
    echo json_encode([
      'status' => 'ok',
      'payload' => $data,
      'message' => $message
    ]);
  }

  /**
   * show data from table
   *
   * @param $table string
   * @param string $id
   */
  public function showData($table = '', $id = '') {

    if (!in_array($table, array_keys(self::$tables))) {
      $this->response([], 'Указанная таблица не существует');
      return;
    }

    $query = "SELECT * FROM $table ";
    if ($id) $query .= ' WHERE ID = `$id`';
    $r = mysqli_query($this->getDb(), $query);
    if ($r !== false) {
      $data = [];
      while ($res = mysqli_fetch_assoc($r)) {
        $data[] = $res;
      }
      $this->response($data);
    } else {
      $this->response([], 'Запрошенные данные не найдены');
    }

  }

  /**
   * Insert data in table
   *
   * @param $table string
   * @param $data array in column=>value
   */
  public function insertData($table, $data) {
    if (!in_array($table, array_keys(self::$tables))) {
      $this->response([], 'Указанная таблица не существует');
      return;
    }

    $fields = [];
    $values = [];
    $requiredFields = self::$tables[$table];
    foreach ($data as $k => $v) {

      $key = array_search($k, $requiredFields);
      if ($key !== false) {
        unset($requiredFields[$key]);
      }

      $fields[] = $k;
      $values[] = "'" . $v . "'";
    }

    if ($requiredFields) {
      $this->response([], 'Не все поля заполнены: ' . implode(', ', $requiredFields));
      return;
    }

    $query = "INSERT INTO $table (" . implode(',', $fields) . ") VALUE (" . implode(',', $values) . ")";
    mysqli_query($this->getDb(), $query);
  }

  /**
   * @param $sessionId
   * @param $userEmail
   */
  public function sessionSubscribe($sessionId, $userEmail) {
    $query = "SELECT ID FROM Participant WHERE Email = '$userEmail' LIMIT 1";
    $userExist = mysqli_query($this->getDb(), $query)->fetch_assoc();
    if (!$userExist) {
      $this->response([], 'Указанный участник не найден');
      return;
    }
    $userId = $userExist['ID'];

    $query = "SELECT Count FROM Session WHERE ID = $sessionId ";
    $sessionExist = mysqli_query($this->getDb(), $query)->fetch_assoc();
    if (!$sessionExist) {
      $this->response([], 'Указанная лекция не найдена');
      return;
    }

    $availableCount = $sessionExist['Count'];
    $query = "SELECT  count(ID) as `count` FROM SessionSubscription WHERE ID = $sessionId ";
    $subscribedCount = mysqli_query($this->getDb(), $query)->fetch_assoc()['count'];

    if ($subscribedCount >= $availableCount) {
      $this->response([], 'Извините, все места заняты');
      return;
    }

    $query = "SELECT ID FROM SessionSubscription WHERE SessionId = $sessionId AND ParticipantId = $userId";
    $alreadyRegistered = mysqli_query($this->getDb(), $query)->fetch_assoc();
    if ($alreadyRegistered) {
      $this->response([], 'Вы уже зарегистрированы на эту лекцию');
      return;
    }

    $this->insertData('SessionSubscription', [
      'ParticipantId' => $userId,
      'SessionId' => $sessionId,
    ]);

    $this->response([], 'Спасибо, вы успешно записаны!');
    return;
  }


  /**
   * Posting news
   *
   * @param $userEmail
   * @param $newsTitle
   * @param $newsMessage
   */
  public function postNews($userEmail, $newsTitle, $newsMessage) {
    $query = "SELECT ID FROM Participant WHERE Email = '$userEmail' LIMIT 1";
    $userExist = mysqli_query($this->getDb(), $query)->fetch_assoc();
    if (!$userExist) {
      $this->response([], 'Указанный пользователь не найден');
      return;
    }
    $userId = $userExist['ID'];

    $query = "SELECT ID FROM News WHERE ParticipantId = '$userId' LIMIT 1";
    $postExist = mysqli_query($this->getDb(), $query)->fetch_assoc();
    if ($postExist) {
      $this->response([], 'У вас уже есть опубликованная новость');
      return;
    }

    $this->insertData('News', [
      'ParticipantId' => $userId,
      'NewsTitle' => $newsTitle,
      'NewsMessage' => $newsMessage,
      'LikesCounter' => '444',
    ]);

    $this->response([], 'Спасибо, ваша новость сохранена!');
  }

  /**
   * Processing request
   */
  public function process() {
    $url = ltrim(strtok($_SERVER["REQUEST_URI"], '?'), '/');

    switch ($url) {
      case 'Table':
        $this->showData($this->param('table'), $this->param('id'));
        break;
      case 'SessionSubscribe':
        $this->sessionSubscribe($this->param('sessionId'), $this->param('userEmail'));
        break;
      case 'PostNews':
        $this->postNews($this->param('userEmail'), $this->param('newsTitle'), $this->param('newsMessage'));
        break;
      case 'InsertData':
        $this->insertData($this->param('table'), $this->param('data'));
        break;
      default:
        return false;
        break;
    };

    return true;

    //$this->insertData('News',
    //  [
    //    'ParticipantId' => '1',
    //    'NewsTitle' => '333',
    //    'NewsMessage' => '33',
    //
    //  ]);

    //$this->showData('News');

    //$this->sessionSubscribe('3', 'qwe@qwe.qwe');
    //$this->sessionSubscribe('1', 'airmail@code-pilots.com');

    //$this->postNews('qwe@qwe.qwe', 'title2', 'text1');

  }

}
