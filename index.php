<?php
include "Api.php";

$api = new Api();
$res = $api->process();
?>

<?php if (!$res): ?>
  <h1>Доступные команды</h1>

  <h2>Table</h2>
  <ul>
    <li><a target="_blank" href="/Table?table=News">Table?&table=News</a></li>
    <li><a target="_blank" href="/Table?table=Participant">Table?&table=Participant</a></li>
    <li><a target="_blank" href="/Table?table=Session">Table?&table=Session</a></li>
    <li><a target="_blank" href="/Table?table=SessionSubscription">Table?&table=SessionSubscription</a></li>
    <li><a target="_blank" href="/Table?table=Speaker">Table?&table=Speaker</a></li>
  </ul>

  <h2>SessionSubscribe</h2>
  <ul>
    <li>
      <a target="_blank" href="/SessionSubscribe?sessionId=1&userEmail=qwe@qwe.qwe"> /SessionSubscribe?&sessionId=1&userEmail=qwe@qwe.qwe</a>
    </li>
  </ul>

  <h2>PostNews</h2>
  <ul>
    <li>
      <a target="_blank" href="/PostNews?userEmail=qwe@qwe.qwe&newsTitle=new_title&newsMessage=message"> /SessionSubscribe?&sessionId=1&userEmail=qwe@qwe.qwe</a>
    </li>
  </ul>

  <h2>InsertData</h2>
  <ul>
    <li>
      <a target="_blank" href="/InsertData?table=News&data[userEmail]=qwe@qwe.qwe&data[newsTitle]=new_title&data[newsMessage]=message"> /InsertData?table=News&data[userEmail]=qwe@qwe.qwe&data[newsTitle]=new_title&data[newsMessage]=message</a>
    </li>
    <li>
      <a target="_blank" href="/InsertData?table=News&data[userEmail]=qwe@qwe.qwe&data[newsTitle]=new_title"> /InsertData?table=News&data[userEmail]=qwe@qwe.qwe&data[newsTitle]=new_title</a>
    </li>
  </ul>

  <h2>О реализации</h2>

  <ul>
    <li>Поскольку задание тестовое, не делал проверку входящих данных</li>
    <li>Для того, чтобы можно было проверить в браузере параметры передаются через $_GET</li>
    <li>Таблицы User не существует - вместо нее используется таблица Participant</li>
    <li>Для проверки заполняемости сессий в таблицу добавлена колонка Count</li>
    <li>Таблица Speaker не используется</li>
    <li>Поскольку задание тестовое, роутинг сделан простым условием и данные получаются вручную для каждой переменной</li>
    <li>Евгений Бобров, yujin1st@gmail.com, skype: yujin1st</li>
  </ul>

<?php endif; ?>

